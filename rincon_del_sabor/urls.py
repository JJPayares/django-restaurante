"""rincon_del_sabor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt import views as jwt_views

from app import api_views

api_router = DefaultRouter()
api_router.register(r'waiters', api_views.WaiterViewSet)
api_router.register(r'clients', api_views.ClientViewSet)
api_router.register(r'tables', api_views.TableViewSet)
api_router.register(r'p_types', api_views.ProductTypeViewSet)
api_router.register(r'products', api_views.ProductViewSet)
api_router.register(r'bills', api_views.BillViewSet)
api_router.register(r'orders', api_views.OrderViewSet)

urlpatterns = [
    # app urls
    path('admin/', admin.site.urls),
    path('', include('app.urls')),

    # DRF urls
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(api_router.urls)),

    # JWT
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
