from django.contrib import admin
from app.models import User, Waiter, Client, Table, Bill, Order, Product, ProductType


# Register your models here.
@admin.register(User)
class AdminUser(admin.ModelAdmin):
    pass


@admin.register(Waiter)
class AdminWaiter(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'last_name2', 'email', 'is_active')


@admin.register(Client)
class AdminClient(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'observations', 'inactive')


@admin.register(Table)
class AdminTable(admin.ModelAdmin):
    list_display = ('numb_clients', 'location', 'inactive')


@admin.register(Bill)
class AdminBill(admin.ModelAdmin):
    list_display = ('id', 'client_id', 'waiter_id', 'bill_date', 'inactive')


@admin.register(Order)
class AdminOrder(admin.ModelAdmin):
    list_display = ('id', 'product', 'count', 'table_id', 'bill_id', 'order_date')


@admin.register(Product)
class AdminProduct(admin.ModelAdmin):
    list_display = ('name', 'type', 'price', 'inactive')


@admin.register(ProductType)
class AdminProductType(admin.ModelAdmin):
    list_display = ('type', 'detail', 'inactive')
