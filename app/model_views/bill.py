from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from app.models import Bill


class BillList(ListView):
    queryset = Bill.objects.filter(inactive=False)
    context_object_name = 'bills_list'
    template_name = 'app/bills/bill_list.html'


class BillDetail(DetailView):
    model = Bill
    context_object_name = 'bills_details'
    template_name = 'app/bills/bill_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['total'] = context['bills_details'].orders.reverse
        context['total'] = 0
        for order in context['bills_details'].orders.reverse():
            context['total'] += order.get_sub_total()
        return context


class BillCreate(CreateView):
    model = Bill
    fields = ['client_id', 'waiter_id']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:bill_list')


class BillUpdate(UpdateView):
    model = Bill
    fields = ['client_id', 'waiter_id']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:bill_list')


class BillDelete(UpdateView):
    model = Bill
    fields = ['inactive']
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:bill_list')
