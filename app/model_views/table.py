from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from app.models import Table



class TableList(ListView):
    queryset = Table.objects.filter(inactive=False)
    context_object_name = 'table_list'
    template_name = 'app/table/table_list.html'


class TableCreate(CreateView):
    model = Table
    fields = ['numb_clients', 'location']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:table_list')


class TableUpdate(UpdateView):
    model = Table
    fields = ['numb_clients', 'location']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:table_list')


class TableDelete(UpdateView):
    model = Table
    fields = ['inactive']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:table_list')
