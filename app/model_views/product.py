from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from app.models import Product


class ProductList(ListView):
    queryset = Product.objects.filter(inactive=False)
    context_object_name = 'product_list'
    template_name = 'app/product/product_list.html'


class ProductCreate(CreateView):
    model = Product
    fields = ['name', 'price', 'type']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:product_list')


class ProductUpdate(UpdateView):
    model = Product
    fields = ['name', 'price', 'type']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:product_list')


class ProductDelete(UpdateView):
    model = Product
    fields = ['inactive']
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:product_list')
