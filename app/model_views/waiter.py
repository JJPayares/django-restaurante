from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from app.forms import WaiterCreateForm
from app.models import Waiter


class WaiterList(ListView):
    queryset = Waiter.objects.filter(is_active=True)
    context_object_name = 'waiter_list'
    template_name = 'app/waiter/waiter_list.html'


class WaiterCreate(CreateView):
    model = Waiter
    form_class = WaiterCreateForm
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:waiter_list')


class WaiterUpdate(UpdateView):
    model = Waiter
    fields = ['first_name', 'last_name', 'last_name2', 'email']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:waiter_list')


class WaiterDelete(UpdateView):
    model = Waiter
    fields = ['is_active']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:waiter_list')
