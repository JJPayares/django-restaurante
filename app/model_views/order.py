from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from app.models import Order


class OrderList(ListView):
    queryset = Order.objects.filter(inactive=False)
    context_object_name = 'orders_list'
    template_name = 'app/order/order_list.html'


class OrderCreate(CreateView):
    model = Order
    fields = ['product', 'count', 'bill_id', 'table_id']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:order_list')


class OrderUpdate(UpdateView):
    model = Order
    fields = ['product', 'count', 'bill_id', 'table_id']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:order_update')


class OrderDelete(UpdateView):
    model = Order
    fields = ['inactive']
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:order_update')
