from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from app.models import ProductType


class ProductTypeList(ListView):
    queryset = ProductType.objects.filter(inactive=False)
    context_object_name = 'product_type_list'
    template_name = 'app/product_type/product_type_list.html'


class ProductTypeCreate(CreateView):
    model = ProductType
    fields = ['type', 'detail']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:p_type_list')


class ProductTypeUpdate(UpdateView):
    model = ProductType
    fields = ['type', 'detail']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:p_type_list')


class ProductTypeDelete(UpdateView):
    model = ProductType
    fields = ['inactive']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:p_type_list')