from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from app.models import Client


class ClientList(ListView):
    queryset = Client.objects.filter(inactive=False)
    context_object_name = 'clients_list'
    template_name = 'app/client/client_list.html'


class ClientCreate(CreateView):
    model = Client
    fields = ['first_name', 'last_name', 'observations']
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:client_list')


class ClientUpdate(UpdateView):
    model = Client
    fields = ['first_name', 'last_name', 'observations']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_form.html'
    success_url = reverse_lazy('app:client_list')


class ClientDelete(UpdateView):
    model = Client
    fields = ['inactive']
    pk_url_kwarg = 'pk'
    template_name = 'base/base_delete_form.html'
    success_url = reverse_lazy('app:client_list')
