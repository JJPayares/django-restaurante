from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

# Users models
class User(AbstractUser):
    pass


class Waiter(User):
    last_name2 = models.CharField(max_length=150, blank=True)

    class Meta:
        verbose_name = 'Waiter'


class Client(models.Model):
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    observations = models.CharField(max_length=45, default='No comments')
    inactive = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        verbose_name = 'Client'


# Tables model
class Table(models.Model):
    numb_clients = models.IntegerField()
    location = models.CharField(max_length=45, null=False, blank=False)
    inactive = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.location} {self.numb_clients}'


class ProductType(models.Model):
    type = models.CharField(max_length=45, null=False, blank=False)
    detail = models.CharField(max_length=255, null=False, blank=False)
    inactive = models.BooleanField(default=False)

    def __str__(self):
        return self.type


# Products models
class Product(models.Model):
    name = models.CharField(max_length=45, null=False, blank=False)
    price = models.FloatField()
    type = models.ForeignKey(ProductType, on_delete=models.CASCADE)
    inactive = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name}'


# Bills model
class Bill(models.Model):
    bill_date = models.DateField(auto_now_add=True, blank=False, null=False)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter_id = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    inactive = models.BooleanField(default=False)

    def __str__(self):
        return f'Factura #{self.id}, cliente {self.client_id.full_name()}'


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    count = models.IntegerField()
    bill_id = models.ForeignKey(Bill, on_delete=models.CASCADE, related_name='orders')
    order_date = models.DateField(auto_now_add=True, blank=False, null=False)
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE)
    inactive = models.BooleanField(default=False)
    sub_total = models.FloatField(default=0)

    def __str__(self):
        return f'orden #{self.id}'

    def get_sub_total(self):
        price = self.product.price
        total = price * self.count
        return total
