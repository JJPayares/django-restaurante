# Generated by Django 4.0.4 on 2022-05-10 02:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_remove_bill_is_active_remove_order_is_active_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='inactive',
            field=models.BooleanField(default=False),
        ),
    ]
