from rest_framework import serializers
from app import models


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.User
        fields = ['url', 'username', 'first_name', 'is_staff']


class WaiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Waiter
        fields = ['id', 'first_name', 'last_name']


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = '__all__'


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Table
        fields = '__all__'


class ProductTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductType
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Product
        fields = "__all__"


class BillSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Bill
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Order
        fields = '__all__'
