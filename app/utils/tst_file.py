EXAMPLE = '/model/view'


def model_from_path(path):
    return path.split('/')[1]


print(model_from_path(EXAMPLE))
