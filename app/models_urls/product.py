from django.urls import path

from app.model_views.product import *

products_urlpatterns = [
    path('products/list/', ProductList.as_view(), name='product_list'),
    path('products/new/', ProductCreate.as_view(), name='product_new'),
    path('products/edit/<int:pk>', ProductUpdate.as_view(), name='product_update'),
    path('products/delete/<int:pk>', ProductDelete.as_view(), name='product_delete'),
]
