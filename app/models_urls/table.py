from django.urls import path

from app.model_views.table import *

tables_urlpatterns = [
    path('tables/list/', TableList.as_view(), name='table_list'),
    path('tables/new/', TableCreate.as_view(), name='table_new'),
    path('tables/edit/<int:pk>', TableUpdate.as_view(), name='table_update'),
    path('tables/delete/<int:pk>', TableDelete.as_view(), name='table_delete'),
]