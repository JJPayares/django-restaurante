from django.urls import path

from app.model_views.client import *

clients_urlpatterns = [
    path('clients/list/', ClientList.as_view(), name='client_list'),
    path('clients/new/', ClientCreate.as_view(), name='client_new'),
    path('clients/edit/<int:pk>', ClientUpdate.as_view(), name='client_update'),
    path('clients/delete/<int:pk>', ClientDelete.as_view(), name='client_delete'),
]