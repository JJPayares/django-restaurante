from django.urls import path

from app.model_views.bill import *

bills_urlpatterns = [
    path('bills/list/', BillList.as_view(), name='bill_list'),
    path('bills/new/', BillCreate.as_view(), name='bill_new'),
    path('bills/edit/<int:pk>', BillUpdate.as_view(), name='bill_update'),
    path('bills/delete/<int:pk>', BillDelete.as_view(), name='bill_delete'),
    path('bills/detail/<int:pk>', BillDetail.as_view(), name='bill_detail'),
]