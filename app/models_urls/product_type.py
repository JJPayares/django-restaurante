from django.urls import path

from app.model_views.product_type import *

p_types_urlpatterns = [
    path('p_types/list/', ProductTypeList.as_view(), name='p_type_list'),
    path('p_types/new/', ProductTypeCreate.as_view(), name='p_type_new'),
    path('p_types/edit/<int:pk>', ProductTypeUpdate.as_view(), name='p_type_update'),
    path('p_types/delete/<int:pk>', ProductTypeDelete.as_view(), name='p_type_delete'),
]
