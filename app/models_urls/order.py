from django.urls import path

from app.model_views.order import *

orders_urlpatterns = [
    path('orders/list/', OrderList.as_view(), name='order_list'),
    path('orders/new/', OrderCreate.as_view(), name='order_new'),
    path('orders/edit/<int:pk>', OrderUpdate.as_view(), name='order_update'),
    path('orders/delete/<int:pk>', OrderDelete.as_view(), name='order_delete'),
]