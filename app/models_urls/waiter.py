from django.urls import path

from app.model_views.waiter import *

waiters_urlpatterns = [
    path('waiters/list/', WaiterList.as_view(), name='waiter_list'),
    path('waiters/new/', WaiterCreate.as_view(), name='waiter_new'),
    path('waiters/edit/<int:pk>', WaiterUpdate.as_view(), name='waiter_update'),
    path('waiters/delete/<int:pk>', WaiterDelete.as_view(), name='waiter_delete'),
]