"""rincon_del_sabor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from app.views import *
from .models_urls.bill import *
from .models_urls.client import *
from .models_urls.order import *
from .models_urls.product import *
from .models_urls.product_type import *
from .models_urls.table import *
from .models_urls.waiter import *

app_name = 'app'

urlpatterns = [
                  path('', home_view, name='index'),
              ] + waiters_urlpatterns \
              + clients_urlpatterns \
              + tables_urlpatterns \
              + p_types_urlpatterns \
              + products_urlpatterns \
              + bills_urlpatterns \
              + orders_urlpatterns
