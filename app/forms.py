from django import forms
from app.models import *
from django.contrib.auth.forms import UserCreationForm


# Waiter Forms
class WaiterCreateForm(UserCreationForm):
    class Meta:
        model = Waiter
        fields = ['first_name', 'last_name', 'last_name2', 'username', 'password1', 'password2',  'email']


class WaiterUpdateForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ['first_name', 'last_name', 'last_name2', 'email']


class WaiterDeleteForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ['is_active']


# Client Forms
class ClientCreateForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['first_name', 'last_name', 'observations']


class ClientUpdateForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['first_name', 'last_name', 'observations']


class ClientDeleteForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['inactive']


# Table forms
class TableCreateForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ['numb_clients', 'location']


class TableUpdateForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ['numb_clients', 'location']


class TableDeleteForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ['inactive']


# Product Type forms
class ProductTypeCreateForm(forms.ModelForm):
    class Meta:
        model = ProductType
        fields = ['type', 'detail']


class ProductTypeUpdateForm(forms.ModelForm):
    class Meta:
        model = ProductType
        fields = ['type', 'detail']


class ProductTypeDeleteForm(forms.ModelForm):
    class Meta:
        model = ProductType
        fields = ['inactive']


# Product forms
class ProductCreateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'price', 'type']


class ProductUpdateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'price', 'type']


class ProductDeleteForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['inactive']


# Bill forms
class BillCreateForm(forms.ModelForm):
    class Meta:
        model = Bill
        fields = ['client_id', 'waiter_id']


class BillUpdateForm(forms.ModelForm):
    class Meta:
        model = Bill
        fields = ['client_id', 'waiter_id']


class BillDeleteForm(forms.ModelForm):
    class Meta:
        model = Bill
        fields = ['inactive']


# Order forms
class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['product', 'count', 'bill_id', 'table_id']


class OrderUpdateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['product', 'count', 'bill_id', 'table_id']


class OrderDeleteForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['inactive']
